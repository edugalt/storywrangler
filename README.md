![storywrangler](resources/storywrangler.png)

In real-time,
Twitter strongly imprints world events, popular culture, and the day-to-day;
Twitter records an ever growing compendium of language use and change;
and Twitter has been shown to enable certain kinds of prediction.
Vitally, and absent from many standard corpora such as books and news archives,
Twitter also encodes popularity and spreading through retweets.

Here, we describe Storywrangler, an ongoing,
day-scale curation of over 100 billion tweets
containing around 1 trillion 1-grams
from 2008 to 2020.
For each day, we break tweets into 1-, 2-, and 3-grams across
150+ languages, record usage frequencies,
and generate Zipf distributions.

We make the data set available through an interactive
time series [viewer](https://storywrangling.org),
and as downloadable time series and daily distributions.

We showcase a few examples of the many possible avenues of study
we aim to enable including
how social amplification can be visualized through 
[contagiograms](https://gitlab.com/compstorylab/contagiograms),
and demonstrate the temporal dynamics of anticipation and memory of events.


## Installation
The [setup.sh](setup.sh) script will create a new conda environment (`storywrangler`) 
and install all required packages listed in [requirements.yml](requirements.yml). 

Run the following commands to get started:
```bash
git clone https://gitlab.com/compstorylab/storywrangler.git
bash storywrangler/setup.sh
```


## Usage
Activate conda environment by running the following command:

```bash 
source activate storywrangler
```

You can run [predict.py](src/predict.py) script to get predictions for some short text.

```shell
usage: predict.py [-h] [-n NGRAMS] [-s SCORE] [-m MODEL] [-e] {text} ...

positional arguments:
  {text, doc}           Arguments for specific data types.
    text                run ngrams parser and fastText LID on short text
    doc                 run ngrams parser and fastText LID on .txt document

optional arguments:
  -e, --emoji           download new codes for emojis from (https://www.unicode.org/) 
                        and nre-compile regex to parse out ngrams (default: False)
  -h, --help            show this help message and exit
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -s SCORE, --score SCORE
                        confidence score threshold for the language identifier
                        (default: 0.25)
```

You can also run [lid.py](src/lid.py) script to select tweets for a given language 
over a single day (Decahose stream format).

```shell
usage: lid.py [-h] [-l LANG] [-m MODEL] [-o OUTDIR] [-e] [--score SCORE] datapth

positional arguments:
  datapth               path to a [day] directory of compressed 15-minutes
                        twitter files (ie. /tmp/2019-01-01)

optional arguments:
  --score SCORE         confidence score threshold for the language identifier
                        (default: 0.25)
  -e, --emoji           download new codes for emojis from
                        (https://www.unicode.org/) and re-compile regex to
                        parse out ngrams (default: False)
  -h, --help            show this help message and exit
  -l LANG, --lang LANG  desired language for tweet selection (default: en)
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model 
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/languages)
```

Use [collect.py](src/collect.py) to run 
both of our language classifier and Ngrams-parser on a given day of tweets (Decahose stream format).
Then, run [aggregate.py](src/aggregate.py) on the output directory to aggregate 
predictions and compute language statistics for that day.
```shell
usage: collect.py [-h] [-e] [-p] [-l] 
                 [-n NGRAMS] [-x CODES] [-i IGNORE]  
                 [-o OUTDIR] [-s SCORE] [-m MODEL] 
                 datapth

positional arguments:
  datapth               path to a [day] directory of compressed 15-minutes
                        twitter files (ie. /tmp/2019-01-01)

optional arguments:
  -e, --emoji           download new codes for emojis from
                        (https://www.unicode.org/) and re-compile regex to
                        parse out ngrams (default: False)
  -h, --help            show this help message and exit
  -i IGNORE, --ignore IGNORE
                        a list of languages to ignore (default: 
                            ja th zh zh-cn zh-tw yue wuu jv su km my lo bo
                        )
  -m MODEL, --model MODEL
                        absolute Path to FastText pre-trained model
                        (default: ../storywrangler/opt/tlid/resources/lid.176.bin)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/ngrams)
  -p, --predictions     keep track of network predictions and save to
                        _predictions.csv.gz (default: False)
  -s SCORE, --score SCORE
                        confidence score threshold for the language identifier
                        (default: 0.25)
  -x CODES, --codes CODES
                        path to (.json) language hashtable 
                        (default: ../storywrangler/opt/tlid/resources/language_codes.json)
```

```shell
usage: aggregate.py [-h] [-l] [-n NGRAMS] [-x CODES] [-o OUTDIR] datapth

positional arguments:
  datapth               path to a [day] directory of ngrams (see `collect_ngrams.py`)

optional arguments:
  -h, --help            show this help message and exit
  -l, --languages       collect language statistics and save to
                        _languages.csv.gz (default: False)
  -n NGRAMS, --ngrams NGRAMS
                        n-grams scheme to use (default: 1)
  -o OUTDIR, --outdir OUTDIR
                        absolute Path to save network predictions 
                        (default: ../storywrangler/ngrams)
  -x CODES, --codes CODES
                        path to (.json) language hashtable 
                        (default: ../storywrangler/opt/tlid/resources/language_codes.json)
```

Alternatively, you can use [parser.py](src/parser.py) to run 
both classification and aggregation at once for a given day of tweets (Decahose stream format).

**Note:** this script is faster but will require more RAM as most operations are done in memory. 

### DISCLAIMER
Our current parser does **NOT** support continuous-script based languages
(e.g., Japanese (ja), Chinese (zh), Thai (th), etc.)


## Resources

- [`lid.176.bin`](resources/lid.176.bin) 
FastText language identification model v0.9.1.
- [`emojis.bin`](resources/emojis.bin) 
A compiled regular expression that matches emojis.
- [`ngrams.bin`](resources/ngrams.bin) 
A compiled regular expression that matches ngrams in tweets.
- [`language_hashtbl.json`](resources/language_hashtbl.json) 
A hash-table to match Twitter's language labels to FastText.
- [`language_codes.json`](resources/language_codes.json) 
A hash-table of numeric codes that matches all languages found in the Decahose.


## Citation
```bibtex
@article{storywrangler,
  title={
    Storywrangler: A massive exploratorium 
    for sociolinguistic, cultural, socioeconomic, and political timelines using Twitter
  },
  author={
    Alshaabi, Thayer and
    Adams, Jane L and 
    Arnold, Michael V and  
    Minot, Joshua R and 
    Dewhurst, David R and 
    Andrew J. Reagan and
    Danforth, Christopher M and 
    Dodds, Peter Sheridan
  },
  year={2020},
  note={Available online at \href{http://arxiv.org/abs/2007.12988}{http://arxiv.org/abs/2007.12988}},
}
```


### Supported Languages

A list of all supported languages can be in [`iso-codes.csv`](resources/iso-codes.csv).

`Language`          |`FastText`|`Twitter`|`Language`           |`FastText`|`Twitter` 
:------------------ |:--------:|:-------:|:------------------- |:--------:|:-------: 
Afrikaans           | af       | -       | Lombard             | lmo      | -      |
Albanian            | sq       | -       | Lower-Sorbian       | dsb      | -      | 
Amharic             | am       | am      | Luxembourgish       | lb       | -      |
Arabic              | ar       | ar      | Macedonian          | mk       | -      |
Aragonese           | an       | -       | Maithili            | mai      | -      |
Armenian            | hy       | hy      | Malagasy            | mg       | -      |
Assamese            | as       | -       | Malayalam           | ml       | ml     |
Asturian            | ast      | -       | Malay               | ms       | msa    | 
Avaric              | av       | -       | Maltese             | mt       | -      |
Azerbaijani         | az       | -       | Manx                | gv       | -      |
Bashkir             | ba       | -       | Marathi             | mr       | mr     |
Basque              | eu       | eu      | Mazanderani         | mzn      | -      |
Bavarian            | bar      | -       | Minangkabau         | min      | -      | 
Belarusian          | be       | -       | Mingrelian          | xmf      | -      |
Bengali             | bn       | bn      | Mirandese           | mwl      | -      |
Bihari              | bh       | -       | Mongolian           | mn       | -      |
Bishnupriya         | bpy      | -       | Nahuatl             | nah      | -      |
Bosnian             | bs       | -       | Neapolitan          | nap      | -      | 
Breton              | br       | -       | Nepali              | ne       | ne     |
Bulgarian           | bg       | bg      | Newari              | new      | -      |
Burmese             | my       | my      | Northern-Frisian    | frr      | -      |
Catalan             | ca       | ca      | Northern-Luri       | lrc      | -      |
Cebuano             | ceb      | -       | Norwegian           | no       | no     |
Cherokee            | -        | chr     | Inuktitut           | -        | iu     |
Central-Bikol       | bcl      | -       | Nynorsk             | nn       | -      | 
Central-Kurdish     | ckb      | ckb     | Occitan             | oc       | -      |
Chavacano           | cbk      | -       | Oriya               | or       | or     | 
Chechen             | ce       | -       | Ossetic             | os       | -      |
Chinese-Simplified  | -        | zh-cn   | Pampanga            | pam      | -      |
Chinese-Traditional | -        | zh-tw   | Panjabi             | pa       | pa     | 
Chinese             | zh       | zh      | Persian             | fa       | fa     |
Chuvash             | cv       | -       | Pfaelzisch          | pfl      | -      |
Cornish             | kw       | -       | Piemontese          | pms      | -      |
Corsican            | co       | -       | Polish              | pl       | pl     |
Croatian            | hr       | -       | Portuguese          | pt       | pt     | 
Czech               | cs       | cs      | Pushto              | ps       | ps     |
Danish              | da       | da      | Quechua             | qu       | -      |
Dimli               | diq      | -       | Raeto-Romance       | rm       | -      |
Divehi              | dv       | dv      | Romanian            | ro       | ro     |
Dotyali             | dty      | -       | Russia-Buriat       | bxr      | -      | 
Dutch               | nl       | nl      | Russian             | ru       | ru     |
Egyptian-Arabic     | arz      | -       | Rusyn               | rue      | -      | 
Emiliano-Romagnolo  | eml      | -       | Sanskrit            | sa       | -      |
English             | en       | en      | Sardinian           | sc       | -      | 
Erzya               | myv      | -       | Saxon               | nds      | -      |
Esperanto           | eo       | -       | Scots               | sco      | -      | 
Estonian            | et       | et      | Serbian             | sr       | sr     |
Fiji-Hindi          | hif      | -       | Serbo-Croatian      | sh       | -      |
Filipino            | -        | fil     | Sicilian            | scn      | -      |
Finnish             | fi       | fi      | Sindhi              | sd       | sd     | 
French              | fr       | fr      | Sinhala             | si       | si     | 
Frisian             | fy       | -       | Slovak              | sk       | -      |
Gaelic              | gd       | -       | Slovenian           | sl       | sl     | 
Gallegan            | gl       | -       | Somali              | so       | -      | 
Georgian            | ka       | ka      | South-Azerbaijani   | azb      | -      |
German              | de       | de      | Spanish             | es       | es     | 
Goan-Konkani        | gom      | -       | Sundanese           | su       | -      | 
Greek               | el       | el      | Swahili             | sw       | -      |
Guarani             | gn       | -       | Swedish             | sv       | sv     |
Gujarati            | gu       | gu      | Tagalog             | tl       | tl     |
Haitian             | ht       | ht      | Tajik               | tg       | -      | 
Hebrew              | he       | he/iw   | Tamil               | ta       | ta     |
Hindi               | hi       | hi      | Tatar               | tt       | -      | 
Hungarian           | hu       | hu      | Telugu              | te       | te     | 
Icelandic           | is       | is      | Thai                | th       | th     |
Ido                 | io       | -       | Tibetan             | bo       | bo     | 
Iloko               | ilo      | -       | Tosk-Albanian       | als      | -      |
Indonesian          | id       | id/in   | Turkish             | tr       | tr     | 
Interlingua         | ia       | -       | Turkmen             | tk       | -      | 
Interlingue         | ie       | -       | Tuvinian            | tyv      | -      | 
Irish               | ga       | -       | Uighur              | ug       | ug     |
Italian             | it       | it      | Ukrainian           | uk       | uk     | 
Japanese            | ja       | ja      | Upper-Sorbian       | hsb      | -      | 
Javanese            | jv       | -       | Urdu                | ur       | ur     |
Kalmyk              | xal      | -       | Uzbek               | uz       | -      | 
Kannada             | kn       | kn      | Venetian            | vec      | -      | 
Karachay-Balkar     | krc      | -       | Veps                | vep      | -      |
Kazakh              | kk       | -       | Vietnamese          | vi       | vi     | 
Khmer               | km       | km      | Vlaams              | vls      | -      | 
Kirghiz             | ky       | -       | Volapük             | vo       | -      |
Komi                | kv       | -       | Walloon             | wa       | -      | 
Korean              | ko       | ko      | Waray               | war      | -      | 
Kurdish             | ku       | -       | Welsh               | cy       | cy     | 
Lao                 | lo       | lo      | Western-Mari        | mrj      | -      |
Latin               | la       | -       | Western-Panjabi     | pnb      | -      | 
Latvian             | lv       | lv      | Wu-Chinese          | wuu      | -      |
Lezghian            | lez      | -       | Yakut               | sah      | -      | 
Limburgan           | li       | -       | Yiddish             | yi       | -      | 
Lithuanian          | lt       | lt      | Yoruba              | yo       | -      | 
Lojban              | jbo      | -       | Yue-Chinese         | yue      | -      | 
