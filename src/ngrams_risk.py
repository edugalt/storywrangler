"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import os
import json

import torch
import pyro
import pyro.distributions as dist

import ngrams_vis
import numpy as np
import pandas as pd

SEED = 0
torch.manual_seed(SEED)
pyro.set_rng_seed(SEED)


def get_word_dfs(data_path, data_files, pct_change=False):
    word_data_files = [x for x in os.listdir(data_path) if x.endswith('json')]
    word_classes = [x.strip('.json') for x in word_data_files]

    dfs = {c: None for c in word_classes}
    for i, f in enumerate(data_files):
        with open(data_path / f, 'r') as file_:
            words = json.load(file_)

        df = dict()
        for name, value in words['data'].items():
            value = np.array(value)
            df[name] = pd.Series(
                value[:, -1].astype(float),
                index=pd.to_datetime(value[:, 0]),
            )
        df = pd.concat(df, axis=1).fillna(method='ffill')
        if pct_change:
            df = df.apply(np.log).diff().dropna()
        dfs[word_classes[i]] = df
    return dfs


def get_monthly_gpr_data(data_path, pct_change=False):
    geo = pd.read_excel(data_path / 'geopolitical_risk_index.xlsx').iloc[:425]
    geo.index = pd.to_datetime(geo.Date)
    gpr = geo.GPR.loc['2009-08-01':]
    if pct_change:
        gpr = gpr.apply(np.log).diff().dropna()
    return gpr


def get_daily_vix_data(data_path):
    vix = pd.read_csv(data_path / 'vix.csv')
    vix['VIXCLS'] = vix['VIXCLS'].replace('.', 'NaN').astype(float)
    vix.index = pd.to_datetime(vix.DATE)
    del vix['DATE']
    vix = vix.loc['2009-08-03':'2020-05-18']
    vix = vix.resample('d').ffill().apply(np.log).diff().ffill().dropna()

    return vix


def sample(model, X, y, ):
    pyro.clear_param_store()
    kernel = pyro.infer.NUTS(model)
    mcmc = pyro.infer.MCMC(kernel, num_samples=2000, warmup_steps=500)
    mcmc.run(X, y)
    samples = {k: v.detach() for k, v in mcmc.get_samples().items()}
    posterior = pyro.infer.Predictive(
        model,
        samples,
        num_samples=2000,
    )
    return samples, posterior


def basic_linear_model(X, y):
    beta = pyro.sample(
        'beta',
        dist.Normal(0., 1.).expand((X.shape[-1] + 1,))
    )
    sigma = pyro.sample(
        'sigma',
        dist.LogNormal(0., 1.)
    )
    with pyro.plate('data', size=X.shape[0]) as n:
        mu = torch.matmul(X[n], beta[1:]) + beta[0]
        obs = pyro.sample(
            'y',
            dist.Normal(mu, sigma),
            obs=y[n] if y is not None else None
        )
    return mu


def gpr_model(datapath, savepath):
    word_data_files = [x for x in os.listdir(datapath) if x.endswith('json')]
    word_dfs = get_word_dfs(datapath, word_data_files)

    gpr = get_monthly_gpr_data(datapath, pct_change=True)
    social_unrest_monthly_design = word_dfs['basic_social_unrest'] \
                                       .apply(np.log).diff().fillna(method='ffill').resample('M').mean().iloc[:-1]
    social_unrest_design_tensor = torch.tensor(social_unrest_monthly_design.values, dtype=torch.float)
    gpr_tensor = torch.tensor(gpr.values, dtype=torch.float)

    train_ind = int(0.75 * gpr_tensor.shape[0])
    gpr_mu = gpr[:train_ind].mean()
    gpr_std = gpr[:train_ind].std()
    gpr = (gpr - gpr_mu) / gpr_std
    su_mu = social_unrest_design_tensor[:train_ind].mean(axis=0).unsqueeze(0)
    su_std = social_unrest_design_tensor[:train_ind].std(axis=0).unsqueeze(0)
    social_unrest_design_tensor = (social_unrest_design_tensor - su_mu) / su_std

    gpr_samples, gpr_posterior = sample(
        basic_linear_model,
        social_unrest_design_tensor[:train_ind],
        gpr_tensor[:train_ind],
    )

    pct_5 = torch.kthvalue(gpr_samples['beta'], int(gpr_samples['beta'].shape[0] * 0.05), dim=0)[0]
    pct_95 = torch.kthvalue(gpr_samples['beta'], int(gpr_samples['beta'].shape[0] * 0.95), dim=0)[0]

    ngrams_vis.plot_risk_hists(
        f'{savepath}/geopolitical_risk_linear_model',
        social_unrest_monthly_design,
        gpr_samples,
        pct_5,
        pct_95
    )
    print(f'Saved: {savepath}/geopolitical_risk_linear_model')


def vix_model(datapath, savepath):
    word_data_files = [x for x in os.listdir(datapath) if x.endswith('json')]
    word_dfs = get_word_dfs(datapath, word_data_files)

    asset_word_design = word_dfs['basic_asset_word'].resample('d').ffill().apply(np.log) \
                            .diff().fillna(method='ffill').loc['2009-08-03':'2020-05-17']

    vix = get_daily_vix_data(datapath)
    asset_word_tensor = torch.tensor(asset_word_design.values, dtype=torch.float)
    vix_tensor = torch.tensor(vix.values.flatten(), dtype=torch.float)

    train_ind = int(0.75 * vix_tensor.shape[0])
    vix_tensor_mu = vix_tensor[:train_ind].mean()
    vix_tensor_std = vix_tensor[:train_ind].std()
    vix_tensor = (vix_tensor - vix_tensor_mu) / vix_tensor_std
    aw_mu = asset_word_tensor[:train_ind].mean(axis=0).unsqueeze(0)
    aw_std = asset_word_tensor[:train_ind].std(axis=0).unsqueeze(0)
    asset_word_tensor = (asset_word_tensor - aw_mu) / aw_std

    vix_samples, vix_posterior = sample(
        basic_linear_model,
        asset_word_tensor[:train_ind],
        vix_tensor[:train_ind].flatten()
    )

    pct_5 = torch.kthvalue(vix_samples['beta'], int(vix_samples['beta'].shape[0] * 0.05), dim=0)[0]
    pct_95 = torch.kthvalue(vix_samples['beta'], int(vix_samples['beta'].shape[0] * 0.95), dim=0)[0]

    ngrams_vis.plot_risk_hists(
        f'{savepath}/vol_linear_model',
        asset_word_design,
        vix_samples,
        pct_5,
        pct_95
    )
    print(f'Saved: {savepath}/vol_linear_model')

