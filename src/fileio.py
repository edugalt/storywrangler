"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""

import struct
import ujson
import csv
import gzip


def get_file_size(gzfile):
    """ Get size of a compressed .gz file
    :param gzfile: path to a compressed file
    :return: size of the uncompressed file
    """
    with open(gzfile, 'rb') as f:
        f.seek(-4, 2)
        return struct.unpack('<I', f.read())[0]


def save_json(data, savepth):
    """ Save a data object to a JSON file
    :param data: a python object
    :param savepth: path to save file
    """
    with open(savepth, "w") as f:
        ujson.dump(data, f, indent=4)


def load_json(pth):
    """ Load a data object from a JSON file
    :param pth: path to a pickle file
    :return: dict
    """
    if pth.suffix == '.json':
        with open(pth, "r") as f:
            return ujson.load(f)
    else:
        print('Error: wrong file format')


def write_csv(path, data, fieldnames, mode='w', sep=','):
    """ Write data to a csv file
    :param path: path to save file
    :param data: a python dict
    :param fieldnames: column names
    :param mode: write/append mode toggle
    :param sep: delimiter to use
    """
    with open(path, mode) as f:
        writer = csv.writer(f, delimiter=sep)

        if mode.startswith('w'):
            writer.writerow(fieldnames)

        for k, v in data.items():
            writer.writerow([k, *v])


def write_gzip_csv(path, data, fieldnames, mode='w', sep=','):
    """ Write data to a compressed csv file
    :param path: path to save file
    :param data: a python dict
    :param fieldnames: column names
    :param mode: write/append mode toggle
    :param sep: delimiter to use
    """
    with gzip.open(path, mode) as f:
        writer = csv.writer(f, delimiter=sep)

        if mode.startswith('w'):
            writer.writerow(fieldnames)

        for k, v in data.items():
            writer.writerow([k, *v])


def write_ngrams_dict(path, data, mode):
    """ Write data to a compressed a JSON file
    :param path: path to save file
    :param data: a python dict
    :param mode: write/append mode toggle
    """
    with gzip.open(path, mode) as f:
        line = ujson.dumps(
                data,
                sort_keys=False,
                ensure_ascii=False,
                escape_forward_slashes=False
            ) + '\n'

        f.write(line.encode('utf-8'))


def save_batch(path, batch, mode):
    """ Write a batch of tweets to a compressed a JSON file
    :param path: path to save file
    :param batch: a python dict
    :param mode: write/append mode toggle
    """
    with gzip.open(path, mode) as f:
        for t in batch:
            line = ujson.dumps(
                t,
                sort_keys=False,
                ensure_ascii=False,
                escape_forward_slashes=False
            ) + '\n'

            f.write(line.encode('utf-8'))
