"""
Storywrangler
Copyright (c) 2020 The Computational Story Lab.
Licensed under the MIT License;
"""


import logging
import shutil
import sys
import time
from pathlib import Path

import cli
import regexr
import ujson
from utils import collect_ngrams


def parse_args(args, config):
    parser = cli.parser()

    parser.add_argument(
        'datapth',
        help='path to a [day] directory of compressed 15-minutes twitter files (ie. /tmp/2019-01-01)'
    )

    # optional args
    parser.add_argument(
        '-n', '--ngrams',
        default=1,
        type=int,
        help='n-grams scheme to use'
    )

    parser.add_argument(
        '-x', '--codes',
        default=Path(config['language_codes']),
        help='path to (.json) language hashtable'
    )

    parser.add_argument(
        '-i', '--ignore',
        default="ja th zh zh-cn zh-tw yue wuu jv su km my lo bo",
        type=str,
        help='a list of languages to ignore (n-grams collection only)'
    )

    parser.add_argument(
        '-m', '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '-o', '--outdir',
        default=Path(config['ngrams']),
        help='absolute Path to save network predictions'
    )

    parser.add_argument(
        '-p', '--predictions',
        action='store_true',
        help='keep track of network predictions and save to _predictions.csv.gz'
    )

    parser.add_argument(
        '-e', '--emoji',
        action='store_true',
        help='download new codes for emojis from (https://www.unicode.org/) and re-compile regex to parse out ngrams'
    )

    parser.add_argument(
        '--score',
        default=float(config['model_threshold']),
        type=float,
        help='confidence score threshold for the language identifier'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    for p in Path(sys.argv[0]).resolve().parents:
        if str(p).endswith('tlid'):
            with open(p/'config.json', 'r') as cfg:
                config = ujson.load(cfg)
                break

    args = parse_args(args, config)

    Path(config["logs"]).mkdir(parents=True, exist_ok=True)
    logfile = f'{config["logs"]}/n{args.ngrams}{Path(args.datapth).stem}.log'
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO, filename=logfile)
    print = logging.info
    logging.info('Initialized logging...')
    logging.info(args)

    if args.emoji:
        regexr.update_parsers(config['twitterlid'])

    eparser = regexr.get_emojis_parser(config['emoji_parser'])
    nparser = regexr.get_ngrams_parser(config['ngrams_parser'])
    outdir = Path(args.outdir)

    done = collect_ngrams(
        args.datapth,
        int(args.ngrams),
        str(args.model),
        eparser,
        nparser,
        outdir,
        args.score,
        args.codes,
        args.ignore.split(' '),
        args.predictions,
    )

    if not done:
        print('Deleting temporary files... ')
        shutil.rmtree(outdir, ignore_errors=True)
        exit()

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {args.outdir}')


if __name__ == "__main__":
    main()
